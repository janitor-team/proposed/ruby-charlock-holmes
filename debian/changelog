ruby-charlock-holmes (0.7.7-3) unstable; urgency=medium

  * debian/control:
    - Drop obsolete XB-Ruby-Versions field.
    - Drop obsolete dependency on ruby-interpreter.
    - Bump Standards-Version to 4.6.1, no changes required.
    - Run wrap-and-sort -abt.
    - Add myself as Uploader.

 -- Georg Faerber <georg@debian.org>  Tue, 25 Oct 2022 08:12:25 +0000

ruby-charlock-holmes (0.7.7-2) unstable; urgency=medium

  [ Debian Janitor ]
  * Apply multi-arch hints. + ruby-charlock-holmes: Add Multi-Arch: same.
  * Remove constraints unnecessary since buster

  [ Pirate Praveen ]
  * Switch to gem-install layout for bundle --local compatibility
  * Bump Standards-Version to 4.6.0 (no changes needed)
  * Bump debhelper compatibility level to 13
  * Update watch file standard to 4

 -- Pirate Praveen <praveen@debian.org>  Mon, 24 Jan 2022 00:32:37 +0530

ruby-charlock-holmes (0.7.7-1) unstable; urgency=medium

  [ Cédric Boutillier ]
  * Update team name
  * Add .gitattributes to keep unwanted files out of the source package

  [ Pirate Praveen ]
  * New upstream version 0.7.7
  * Bump Standards-Version to 4.5.1 (no changes needed)

 -- Pirate Praveen <praveen@debian.org>  Fri, 20 Nov 2020 18:37:43 +0530

ruby-charlock-holmes (0.7.6-2) unstable; urgency=medium

  [ Utkarsh Gupta ]
  * Add salsa-ci.yml

  [ Debian Janitor ]
  * Trim trailing whitespace.
  * Use secure URI in Homepage field.
  * Bump debhelper from old 11 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

  [ Pirate Praveen ]
  * Bump minimum version of gem2deb to 1.0
  * Bump Standards-Version to 4.5.0 (no changes needed)

 -- Pirate Praveen <praveen@debian.org>  Mon, 13 Jul 2020 16:00:42 +0530

ruby-charlock-holmes (0.7.6-1) unstable; urgency=medium

  * New upstream version 0.7.6
    (This version is compatible with ICU 63.1) (Closes: #913510)
  * Bump Standards-Version to 4.2.1 (no changes needed)
  * Move debian/watch to gemwatch.debian.net

 -- Pirate Praveen <praveen@debian.org>  Mon, 19 Nov 2018 18:47:44 +0530

ruby-charlock-holmes (0.7.5-1) unstable; urgency=medium

  [ Cédric Boutillier ]
  * Remove version in the gem2deb build-dependency
  * Use https:// in Vcs-* fields
  * Use https:// in Vcs-* fields
  * Run wrap-and-sort on packaging files

  [ Pirate Praveen ]
  * New upstream version 0.7.5
  * Remove gbp.conf filter (non dfsg file is no longer preset in upstream)
  * Bump debhelper compatibility level to 11
  * Bump Standards-Version to 4.1.3 (no changes needed)

 -- Pirate Praveen <praveen@debian.org>  Sun, 18 Mar 2018 20:12:44 +0530

ruby-charlock-holmes (0.7.3+dfsg-2) unstable; urgency=medium

  * Add zlib1g-dev to build-deps (Closes: #808526)

 -- Pirate Praveen <praveen@debian.org>  Tue, 12 Jan 2016 00:28:06 +0530

ruby-charlock-holmes (0.7.3+dfsg-1) unstable; urgency=medium

  [ Pirate Praveen ]
  * Team upload
  * New upstream release
  * Remove patch use-system-libmagic.patch (already applied)

  [ Balasankar C]
  * Repack source using repack.sh
  * Bump debhelper compatibility to 9
  * Add gbp.conf for future updates

 -- Pirate Praveen <praveen@debian.org>  Fri, 27 Nov 2015 15:01:57 +0530

ruby-charlock-holmes (0.6.9.4.dfsg1-1) unstable; urgency=low

  * Initial release (Closes: #705482)
  * Imported Upstream version 0.6.9.4.dfsg1
  * Add dversionmangling to debian/watch
  * Try without ruby-chardet

 -- Ondřej Surý <ondrej@debian.org>  Mon, 15 Apr 2013 17:14:24 +0200
